package com.clevertec.task3.ui

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.clevertec.task3.R
import com.clevertec.task3.data.Contact

class ContactPickerDialog(
    private val contacts: Array<Contact>,
    private val displayContactFun: (Contact) -> Unit
) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val errorString = getString(R.string.no_phone)
            AlertDialog.Builder(it)
                .setItems(contacts.map { it.phone ?: errorString }.toTypedArray()) { _, which ->
                    displayContactFun(contacts[which])
                }
                .create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}