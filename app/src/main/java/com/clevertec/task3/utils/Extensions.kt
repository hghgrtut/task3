package com.clevertec.task3.utils

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.clevertec.task3.R
import com.clevertec.task3.data.Contact

private const val KEY_PHONE = "phone"

private const val NOTIFICATION_CHANNEL_ID = "11"
private const val NOTIFICATION_ID = 11

fun Activity.showNotification(contact: Contact) {
    val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val notificationChannel = NotificationChannel(
            NOTIFICATION_CHANNEL_ID,
            getString(R.string.numbers),
            NotificationManager.IMPORTANCE_HIGH)
        notificationChannel.description = getString(R.string.numbers_from_sp)
        notificationManager.createNotificationChannel(notificationChannel)
    }
    val notification = NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL_ID)
        .setAutoCancel(true)
        .setSmallIcon(R.drawable.ic_launcher_foreground)
        .setContentTitle(contact.name)
        .setContentText(contact.phone)
        .build()
    notificationManager.notify(NOTIFICATION_ID, notification)
}

fun Context.showToast(@StringRes id: Int) = Toast.makeText(this, id, Toast.LENGTH_SHORT).show()

fun Context.isPermissionGranted() =
    ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS) ==
            PackageManager.PERMISSION_GRANTED

fun Activity.grantPermission() {
    ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_CONTACTS), 1)
}

fun SharedPreferences.spphone(): String = getString(KEY_PHONE, "")!!

fun SharedPreferences.savePhone(phone: String) = edit().putString(KEY_PHONE, phone).apply()

fun TextView.setContactData(@StringRes stringForFormat: Int, data: String?) {
    text = if (data == null) "" else context.getString(stringForFormat, data)
}