package com.clevertec.task3.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

const val TABLE_CONTACT = "table_contact"
const val COLUMN_PHONE = "phone"

@Entity(tableName = TABLE_CONTACT)
data class Contact(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "contact_id") val id: Int?,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "email") val email: String?,
    @ColumnInfo(name = COLUMN_PHONE) val phone: String?)