package com.clevertec.task3

import android.database.Cursor
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import com.clevertec.task3.databinding.ActivityMainBinding
import androidx.activity.result.contract.ActivityResultContracts
import com.clevertec.task3.data.Contact
import com.clevertec.task3.database.MyRoomDatabase
import com.clevertec.task3.ui.ContactPickerDialog
import com.clevertec.task3.utils.grantPermission
import com.clevertec.task3.utils.isPermissionGranted
import com.clevertec.task3.utils.savePhone
import com.clevertec.task3.utils.setContactData
import com.clevertec.task3.utils.showNotification
import com.clevertec.task3.utils.showToast
import com.clevertec.task3.utils.spphone
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainActivity : AppCompatActivity() {
    private var _binding: ActivityMainBinding? = null
    private val binding: ActivityMainBinding get() = requireNotNull(_binding)

    private val dao by lazy { MyRoomDatabase.getDatabase(applicationContext).contactDao() }
    private val prefs by lazy { applicationContext.getSharedPreferences("PREFS", MODE_PRIVATE) }

    private val takeContactCallback = registerForActivityResult(ActivityResultContracts.PickContact()) { uri ->
        if (uri == null) return@registerForActivityResult
        var name: String? = null
        var email: String? = null
        var phone: String? = null
        var cursor: Cursor = contentResolver
            .query(uri, arrayOf(ContactsContract.Contacts.DISPLAY_NAME), null, null, null)!!
        if (cursor.moveToFirst()) {
            val nameColumnIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
            name = cursor.getString(nameColumnIndex)
            cursor.close()

            cursor = contentResolver.query(
                ContactsContract.Data.CONTENT_URI,
                arrayOf(
                    ContactsContract.Data.DISPLAY_NAME,
                    ContactsContract.Contacts.Data.DATA1,
                    ContactsContract.Contacts.Data.MIMETYPE
                ),
                ContactsContract.Data.DISPLAY_NAME + " = ?",
                arrayOf(name),
                null
            )!!
            if (cursor.moveToFirst()) {
                val dataColumnIndex = cursor.getColumnIndex(ContactsContract.Contacts.Data.DATA1)
                val mimeColumnIndex = cursor.getColumnIndex(ContactsContract.Contacts.Data.MIMETYPE)
                val mime = cursor.getString(mimeColumnIndex)
                if (ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE.equals(mime, true)) {
                    email = cursor.getString(dataColumnIndex)
                }
                if (ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE.equals(mime, true)) {
                    phone = cursor.getString(dataColumnIndex)
                }
            }
        }
        cursor.close()
        CoroutineScope(Dispatchers.IO).launch {
            dao.insert(Contact(null, name, email, phone))
            MainScope().launch { showToast(R.string.saved) }
        }
    }

    private val displayAndSaveContact: (Contact) -> Unit = { contact ->
        binding.name.setContactData(R.string.name, contact.name)
        binding.phone.setContactData(R.string.phone, contact.phone)
        binding.email.setContactData(R.string.email, contact.email)
        if (contact.phone != null) prefs.savePhone(contact.phone)
        else showToast(R.string.error_no_phone)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.chooseContact.setOnClickListener {
            if (isPermissionGranted()) takeContactCallback.launch(null) else grantPermission()
        }
        binding.showAll.setOnClickListener { MainScope().launch { showContactsDialog() } }
        binding.fromSp.setOnClickListener {
            Snackbar.make(binding.layout, prefs.spphone(), Snackbar.LENGTH_LONG).show()
        }
        binding.notification.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                val contacts = dao.getContactsByPhone(prefs.spphone())
                MainScope().launch {
                    if (contacts.isEmpty()) showToast(R.string.no_contact)
                    else showNotification(contacts.first())
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private suspend fun showContactsDialog() {
        val dialog = ContactPickerDialog(withContext(Dispatchers.IO) { dao.getAll() }, displayAndSaveContact)
        dialog.show(supportFragmentManager, "myDialog")
    }
}