package com.clevertec.task3.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.clevertec.task3.data.COLUMN_PHONE
import com.clevertec.task3.data.Contact
import com.clevertec.task3.data.TABLE_CONTACT

@Dao
interface ContactDao {
    @Insert(onConflict = REPLACE)
    fun insert(contact: Contact)

    @Query("SELECT * FROM $TABLE_CONTACT")
    fun getAll(): Array<Contact>

    @Query("SELECT * FROM $TABLE_CONTACT WHERE $COLUMN_PHONE = :phone")
    fun getContactsByPhone(phone: String): Array<Contact>
}